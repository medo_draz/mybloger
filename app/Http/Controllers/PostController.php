<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function index(Request $request)
    {
    
        $categories = Category::all();
        $posts = Post::when($request->search, function ($q) use ($request) {

            return $q->whereTranslationLike('title', '%' . $request->search . '%');

        })->when($request->id, function ($q) use ($request) {

            return $q->where('id', $request->id);

        })->latest()->paginate(10);



//        dd($posts);
        return view('pages.article',compact('posts','categories'));
    }

    public function viewcat(Request $request){
        $categories = Category::all();
        $posts = Post::when($request->search, function ($q) use ($request) {

            return $q->whereTranslationLike('title', '%' . $request->search . '%');

        })->when($request->id, function ($q) use ($request) {

            return $q->where('id', $request->id);

        })->latest()->paginate(10);
        return view('pages.categories',compact('categories','posts'));
    }

    public function viewpostofcat(Request $request){
        $categories = Category::all();

        $posts = Post::when($request->search, function ($q) use ($request) {

            return $q->whereTranslationLike('title', '%' . $request->search . '%');

        })->when($request->category_id, function ($q) use ($request) {

            return $q->where('category_id', $request->category_id);

        })->latest()->paginate(5);

        return view('welcome',compact('posts','categories'));
    }




}
