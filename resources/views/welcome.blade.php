@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="box-body">

            @if ($posts->count() > 0)

                <div class="card-columns text-center ">
                    @foreach($posts as $index=>$post)
                        @if($post->status == 'published')
                            {{--<div class="row">
                                <div class="col-la-3 col-md-6">
                                    <img class="card-img-top" src="{{$post->image_path}}" alt="Card image">
                                    <h7 class="card-date">{{Carbon\Carbon::parse($post->created_at)->toFormattedDateString()}}</h7>

                                    <h5 class="card-text">{{$post->category->name}}</h5>

                                    <a href="{{ "/pages/article/".$post->id }}"><h3 class="card-title">{{$post->title}}</h3></a>

                                </div>

                            </div>--}}

                            <div class="card" style="width:350px">
                                <img class="card-img-top" src="{{$post->image_path}}" alt="Card image">

                                <div class="card-body">

                                    <h7 class="card-date">{{Carbon\Carbon::parse($post->created_at)->toFormattedDateString()}}</h7>

                                    <h5 class="card-text">{{$post->category->name}}</h5>

                                    <a href="{{ "/pages/article/".$post->id }}"><h3 class="card-title">{{$post->title}}</h3></a>

                                </div>
                            </div>


                        @endif
                    @endforeach


                    @else

                        <h2>@lang('site.no_data_found')</h2>

                    @endif
                </div><!-- end of box body -->

                <div>
                    {{ $posts->appends(request()->query())->links() }}
                </div>
        </div>






@endsection
