@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="box box-primary">

            @include('partials._errors')

            @foreach($posts as $index=>$post)

                @if($post->id == $post->id)


                    <div>
                        <img src="{{ $post->image_path }}" style="width: 1000px" class="img-thumbnail image-preview"
                             alt="">
                    </div>

                    <div class="post-header">
                        <ul>
                            <li class="byline">{{Carbon\Carbon::parse($post->created_at)->toFormattedDateString()}}</li>
                            <li class="category">{{ $post->category->name }}</li>
                        </ul>
                        <h1>{{ $post->title }}</h1>
                    </div>
                    <div>
                        <p>{!! $post->body !!} </p>
                    </div>
                @endif

            @endforeach


        </div><!-- end of box body -->

    </div><!-- end of box -->






@endsection
