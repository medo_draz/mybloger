<?php

return [
    'dashboard' => 'dashboard',
    'logout' => 'logout',

    'add' => 'add',
    'create' => 'create',
    'read' => 'read',
    'edit' => 'edit',
    'update' => 'update',
    'delete' => 'delete',
    'search' => 'search',
    'show' => 'show',
    'loading' => 'loading',
    'print' => 'print',

    'confirm_delete' => 'confirm delete',
    'yes' => 'yes',
    'no' => 'no',

    'login' => 'login',
    'remember_me' => 'remember me',
    'password' => 'password',
    'password_confirmation' => 'password confirmation',

    'added_successfully' => 'added successfully',
    'updated_successfully' => 'updated successfully',
    'deleted_successfully' => 'deleted successfully',

    'no_data_found' => 'no data found!!',
    'no_records' => 'no records!!',

    'users' => 'users',


    'permissions' => 'permissions',

    'categories' => 'categories',
    'all_categories' => 'all categories',
    'name' => 'name',
    'description' => 'description',
    'products_count' => 'products count',
    'related_products' => 'related products',
    'category' => 'category',
    'show_products' => 'show products',
    'created_at' => 'created at',

    'products' => 'products',
    'product' => 'product',

    'articles' => 'articles',
    'posts'=>'posts',
    'article' => 'article',
    'body' => 'body',
    'status' => 'status',
    'title' => 'title',

    'quantity' => 'quantity',
    'total' => 'total',
    'purchase_price' => 'purchase price',
    'price' => 'price',
    'sale_price' => 'sale price',
    'stock' => 'stock',
    'profit_percent' => 'profit percent',

    'ar' => [
        'name' => 'name in arabic',
        'description' => 'description in english',
        'title' => 'title in arabic',
        'body' => 'body in english',
    ],

    'en' => [
        'name' => 'name in arabic',
        'description' => 'description in english',
        'title' => 'title in arabic',
        'body' => 'body in english',
    ],

];
